package com.example.tonystark.premierleaguelivescore;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

public class TopscorerActivity extends Activity {

    private ImageButton liveButton;
    private ImageButton tableButton;
    private ImageButton newsButton;
    private ImageView backgroundTopscorer;
    private TextView topscorer;
    private ImageButton resetButton;
    private TextView statusConnect;

    private GridView gridView;

    private List<Topscorer> topscorers;
    private TopscorerAdapter topscorerAdapter;

    private static List<String> code = new ArrayList<String>();
    private static List<String> code2 = new ArrayList<String>();

    private static List<String> name = new ArrayList<String>();
    private static List<String> name2 = new ArrayList<String>();

    private static List<String> team = new ArrayList<String>();
    private static List<String> team2 = new ArrayList<String>();

    private static List<String> goal = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topscorer_page);
        initComponents();
    }

    protected void onStart() {
        super.onStart();
        liveButton.setImageResource(R.drawable.livescore3);
        tableButton.setImageResource(R.drawable.table);
        newsButton.setImageResource(R.drawable.news);
    }

    private void initComponents()
    {
        backgroundTopscorer = (ImageView) findViewById(R.id.background_topscorer);
        topscorer = (TextView) findViewById(R.id.topscorer_header);
        topscorers = new ArrayList<Topscorer>();
        topscorerAdapter = new TopscorerAdapter(this, R.layout.topscorer_cell, topscorers );
        gridView = (GridView)findViewById(R.id.topscorer_grid_view);
        gridView.setAdapter(topscorerAdapter);
        liveButton = (ImageButton) findViewById(R.id.livescoreButton);
        liveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                liveButton.setImageResource(R.drawable.livescoreclick);
                playSound();
                Intent intent = new Intent(TopscorerActivity.this, LiveActivity.class);
                startActivity(intent);
            }
        });
        tableButton = (ImageButton) findViewById(R.id.tableButton);
        tableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tableButton.setImageResource(R.drawable.tableclick);
                playSound();
                Intent intent = new Intent(TopscorerActivity.this, TableActivity.class);
                startActivity(intent);
            }
        });
        newsButton = (ImageButton) findViewById(R.id.newsButton);
        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newsButton.setImageResource(R.drawable.newsclick);
                playSound();
                Intent intent = new Intent(TopscorerActivity.this, NewsActivity.class);
                startActivity(intent);
            }
        });
        topscorerCheckInternetAlertMessage();
    }

    public void topscorerCheckInternetAlertMessage(){
        if(isInternetconnected())//Run if internect connected
        {
            statusConnect = (TextView) findViewById(R.id.statusConnection3);
            statusConnect.setVisibility(View.GONE);
            resetButton = (ImageButton) findViewById(R.id.refreshButton3);
            resetButton.setVisibility(View.GONE);
            backgroundTopscorer.setVisibility(View.VISIBLE);
            topscorer.setVisibility(View.VISIBLE);
            runSystem();
        }
        else // Run if no internet connection
        {
            statusConnect = (TextView) findViewById(R.id.statusConnection3);
            statusConnect.setVisibility(View.VISIBLE);
            resetButton = (ImageButton) findViewById(R.id.refreshButton3);
            resetButton.setVisibility(View.VISIBLE);
            backgroundTopscorer.setVisibility(View.GONE);
            topscorer.setVisibility(View.GONE);
            AlertDialog alertDialog = new AlertDialog.Builder(TopscorerActivity.this).create();
            alertDialog.setTitle("CONNEXTION ERROR");

            alertDialog.setMessage("Cannot connected to internet. please try to connect the internet   ");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            statusConnect.setText("NO INTERNET" + "\n" + "CONNECTION");
            resetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    resetButton.setImageResource(R.drawable.refreshclick);
                    playSound();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            });
        }
    }

    /**
     * check if internet connected or not
     */
    public boolean isInternetconnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    /**
     * Load html code from url(live score info) gather info and run gridview
     */
    private void runSystem() {
        Ion.with(getApplicationContext())
                .load("http://www.bbc.com/sport/football/premier-league/top-scorers/")
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        resetList();
                        code.add(result);   //add html code line by line to List<String>code
                        madeWordCode();
                        callName();
                        callTeam();
                        callGoal();
//                        testInfo();
                        if(name2!=null) {
                            for (int i = 0; i < name2.size(); i++) {
                                Topscorer topscorer = new Topscorer(name2.get(i).toString(), team2.get(i).toString()
                                        , goal.get(i).toString());
                                topscorers.add(topscorer);
                            }
                            topscorerAdapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    private void resetList(){
        code.clear();
        code2.clear();
        name.clear();
        name2.clear();
        team.clear();
        team2.clear();
        goal.clear();
    }

    /**
     * split word in each line to code2
     */
    private void madeWordCode()
    {
        for (int i = 0; i < code.size(); i++) {
            String keep = "";
            for (int j = 0; j < code.get(i).length(); j++) {
                if (code.get(i).charAt(j) == ' ') {
                    code2.add(keep);
                    keep = "";
                } else {
                    keep += code.get(i).charAt(j);
                }
            }
        }
    }

    /**
     * get name from code2
     */
    private void callName()
    {
        for(int i=0; i<code2.size(); i++)
        {
            List<String> dummies = new ArrayList<String>();
            dummies.add("gel-double-pica\">");
            String nameText = "";
            String string = code2.get(i);
            boolean stop = false;
            if (code2.get(i).contains(dummies.get(0)))
            {
//                System.out.println(i);
                for(int j=0; j<string.length(); j++)
                {
                    if(string.charAt(j)=='>')
                    {
                        for(int k=j+1; k<string.length() ; k++)
                        {
                            nameText += string.charAt(k);
                        }
                    }
                }
                nameText += " ";
                String string2 = code2.get(i+1);
                for(int j=0; j<string2.length(); j++)
                {
                    if(string2.charAt(j)=='<')
                    {
                        stop = true;
                    }
                    if(!stop)
                    {
                        nameText += string2.charAt(j);
                    }
                }
                name.add(nameText);
            }
        }

        for(int i=0; i<name.size(); i++)
        {
            if(i%2==0)
                name2.add(name.get(i));
        }

    }

    /**
     * get statusMatch from code2
     */
    private void callTeam()
    {
        for(int i=0; i<code2.size(); i++)
        {
            List<String> dummies = new ArrayList<String>();
            dummies.add("team-short-name\">");
            String teamText = "";
            String string = code2.get(i);
            boolean stop = false;
            if (code2.get(i).contains(dummies.get(0)))
            {
                for(int j=0; j<string.length(); j++)
                {
                    if(string.charAt(j)=='>') {
                        for(int k=j+1; k<string.length(); k++) {
                            if(string.charAt(j)=='<')
                                stop = true;
                            if(!stop)
                                teamText += string.charAt(k);
                        }
                    }
                }
                String nextText = code2.get(i+1);
                if(nextText.charAt(0)!='<')
                    teamText += nextText;
                team.add(teamText);
            }
        }

        for(int i=0; i<team.size(); i++)
        {
            String string = team.get(i);
            String teamText = "";
            boolean stop = false;
            for(int j=0; j<string.length(); j++)
            {
                if(string.charAt(j)=='<')
                    stop = true;
                if(!stop)
                    teamText += string.charAt(j);
            }
            team2.add(teamText);
        }
    }

    /**
     * get score from code2
     */
    private void callGoal()
    {
        for(int i=0; i<code2.size()-1; i++)
        {
            List<String> dummies = new ArrayList<String>();
            dummies.add("class=\"top-player-stats__goals-scored\"><span");
            String goalText = "";
            String string = code2.get(i+1);
            char array[] = {'0','1','2','3','4','5','6','7','8','9'};
            if (code2.get(i).contains(dummies.get(0))) {
                for(int j=0; j<string.length(); j++)
                {
                    for(int k=0; k<array.length; k++) {
                        if (string.charAt(j) == array[k])
                            goalText += string.charAt(j);
                    }
                }
                goal.add(goalText);
            }
        }
    }

    /**
     * test all info from url
     */
    private void testInfo()
    {
//        //Test code2
//        for(int i=1800; i<=2200; i++)
//        {
//            System.out.println(code2.get(i));
//        }
        //Test name2
        for(int i=0; i<name2.size(); i++)
        {
            System.out.println(name2.get(i));
        }
        //Test team
        for(int i=0; i<team2.size(); i++)
        {
            System.out.println(team2.get(i));
        }
        //Test goal
        for(int i=0; i<goal.size(); i++)
        {
            System.out.println(goal.get(i));
        }
    }

    public void playSound(){
        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.click);
        mp.start();
    }
}
