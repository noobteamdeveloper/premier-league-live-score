package com.example.tonystark.premierleaguelivescore;

import java.io.Serializable;

public class Live implements Serializable {

    private String matchLeft;
    private String matchRight;
    private String scoreLeft;
    private String scoreRight;
    private String status;

    public Live(String matchleft,String matchright,String scoreleft,String scoreright,String status) {
        this.matchLeft = matchleft;
        this.matchRight = matchright;
        this.scoreLeft = scoreleft;
        this.scoreRight = scoreright;
        this.status = status;
    }


    public String getStatus() { return status; }

    public String getScoreLeft() { return scoreLeft; }

    public String getScoreRight() { return scoreRight; }

    public String getMatchLeft() { return matchLeft; }

    public String getMatchRight() { return matchRight; }

    @Override
    public String toString() {
        return "Live{" +
                "matchLeft='" + matchLeft + '\'' +
                ", matchRight='" + matchRight + '\'' +
                ", scoreLeft='" + scoreLeft + '\'' +
                ", scoreRight='" + scoreRight + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}