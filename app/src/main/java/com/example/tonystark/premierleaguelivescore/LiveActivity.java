package com.example.tonystark.premierleaguelivescore;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

public class LiveActivity extends Activity {

    private ImageButton tableButton;
    private ImageButton topscorerButton;
    private ImageButton newsButton;
    private ImageView backgroundDate;
    private TextView dateMatch;
    private ImageButton resetButton;
    private TextView statusConnect;

    private GridView gridView;

    private List<Live> lives;
    private LiveAdapter liveAdapter;

    private static List<String> code = new ArrayList<String>();//for add html code line by line to List<String>code
    private static List<String> code2 = new ArrayList<String>(); //for split every words(html code)

    private static List<String> code3 = new ArrayList<String>();//for select line that contain match info
//    private static List<Integer> numMatchPerDay = new ArrayList<Integer>();
    private static List<String> matchLeft = new ArrayList<String>();//for add home team
    private static List<String> matchRight = new ArrayList<String>();//for add away team

    private static List<String> day = new ArrayList<String>();//for add date that match play
//    private static List<String> score = new ArrayList<String>();
    private static List<String> scoreLeft = new ArrayList<String>();//for add home score in matches
    private static List<String> scoreRight = new ArrayList<String>();//for add away score in matches
    private static List<String> statusMatch = new ArrayList<String>();//for add status of each match (FT-fulltime or 18.00-now playing)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_page);
        initComponents();
    }

    protected void onStart() {
        super.onStart();
        tableButton.setImageResource(R.drawable.table);
        topscorerButton.setImageResource(R.drawable.topscorer);
        newsButton.setImageResource(R.drawable.news);
        resetButton.setImageResource(R.drawable.refresh);
    }

    private void initComponents()
    {
        backgroundDate = (ImageView) findViewById(R.id.background_date);
        dateMatch = (TextView) findViewById(R.id.date_match);
        lives = new ArrayList<Live>();
        liveAdapter = new LiveAdapter(this, R.layout.live_cell, lives );
        gridView = (GridView)findViewById(R.id.lives_grid_view);
        gridView.setAdapter(liveAdapter);
        tableButton = (ImageButton) findViewById(R.id.tableButton);
        tableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tableButton.setImageResource(R.drawable.tableclick);
                playSound();
                Intent intent = new Intent(LiveActivity.this, TableActivity.class);
                startActivity(intent);
            }
        });
        topscorerButton = (ImageButton) findViewById(R.id.topscorerButton);
        topscorerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                topscorerButton.setImageResource(R.drawable.topscorerclick);
                playSound();
                Intent intent = new Intent(LiveActivity.this, TopscorerActivity.class);
                startActivity(intent);
            }
        });
        newsButton = (ImageButton) findViewById(R.id.newsButton);
        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newsButton.setImageResource(R.drawable.newsclick);
                playSound();
                Intent intent = new Intent(LiveActivity.this, NewsActivity.class);
                startActivity(intent);
            }
        });
        liveCheckInternetAlertMessage();
    }

    /**
     * checkInternet and show alert message
     */
    public void liveCheckInternetAlertMessage(){
        if(isInternetconnected())//Run if internect connected
        {
            statusConnect = (TextView) findViewById(R.id.statusConnection);
            statusConnect.setVisibility(View.GONE);
            resetButton = (ImageButton) findViewById(R.id.refreshButton);
            resetButton.setVisibility(View.GONE);
            backgroundDate.setVisibility(View.VISIBLE);
            dateMatch.setVisibility(View.VISIBLE);
            runSystem();
        }
        else // Run if no internet connection
        {
            statusConnect = (TextView) findViewById(R.id.statusConnection);
            statusConnect.setVisibility(View.VISIBLE);
            resetButton = (ImageButton) findViewById(R.id.refreshButton);
            resetButton.setVisibility(View.VISIBLE);
            backgroundDate.setVisibility(View.GONE);
            dateMatch.setVisibility(View.GONE);
            AlertDialog alertDialog = new AlertDialog.Builder(LiveActivity.this).create();
            alertDialog.setTitle("CONNEXTION ERROR");

            alertDialog.setMessage("Cannot connected to internet. please try to connect the internet   ");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            statusConnect.setText("NO INTERNET" + "\n" + "CONNECTION");
            resetButton = (ImageButton) findViewById(R.id.refreshButton);
            resetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    resetButton.setImageResource(R.drawable.refreshclick);
                    playSound();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            });
        }
    }

    /**
     * check if internet connected or not
     */
    public boolean isInternetconnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    /**
     * Load html code from url(live score info) gather info and run gridview
     */
    private void runSystem() {
        Ion.with(getApplicationContext())
                .load("http://www.livescore.com/soccer/england/premier-league/")
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        resetList();
//                        String s = Html.fromHtml(result).toString();
                        code.add(result);   //add html code line by line to List<String>code
//                        madeWordCode();
//                        madeInfo();
//                        madeScore();
                        madeWordCode();
                        callMatch();
                        callDay();
                        callScore();
                        callStatusMatch();

                        dateMatch.setText(day.get(0));
                        testInfo();
                        if (matchLeft != null) {
                            for (int i = 0; i < matchLeft.size(); i++) {
                                Live live = new Live(matchLeft.get(i).toString(), matchRight.get(i).toString()
                                        , scoreLeft.get(i).toString(), scoreLeft.get(i).toString(), statusMatch.get(i).toString());
                                LiveStorage.getInstance().saveLive(live);
                                lives.add(live);
                            }
                            liveAdapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    private void resetList(){
        code.clear();
        code2.clear();
        code3.clear();
        matchLeft.clear();
        matchRight.clear();
        day.clear();
        scoreLeft.clear();
        scoreRight.clear();
        statusMatch.clear();
    }

    /**
     * split word in each line to code2
     */
    public void madeWordCode()
    {
        for (int i = 0; i < code.size(); i++) {
            String keep = "";
            for (int j = 0; j < code.get(i).length(); j++) {
                if (code.get(i).charAt(j) == ' ') {
                    code2.add(keep);
                    keep = "";
                } else {
                    keep += code.get(i).charAt(j);
                }
            }
        }
//        List<String> newList = new ArrayList<String>();
//        boolean startAdd = false;
//        for(int i=100;i<code2.size();i++)
//        {
//            if(code2.get(i)=="-") startAdd = true;
//            if(code2.get(i)=="Table") break;
//            if(startAdd) newList.add(code2.get(i));
//        }
//
//        code2.clear();
//        code2.addAll(newList);
    }

//    public void madeInfo()
//    {
//        String []month = {"January","February","March","April","May","June","July",
//                            "August","September","October","November","December"};
//        int numberOfMatch = 0;
//        for(int i=0;i<code2.size();i++)
//        {
//            for(int j=0;j<month.length;j++) {
//                if (code2.get(i).equalsIgnoreCase(month[j])) {
//                    day.add(code2.get(i));
//                    numberOfMatch = 0;
//                    int index = 1;
//                    int repeatIndex = 1;
//                    while(true)
//                    {
//                        if(code2.get(i).equals("England - Premier League - Table"))
//                        {
//                            if(day.size()>0) numMatchPerDay.add(numberOfMatch);
//                            break;
//                        }
//                        if (code2.get(i).equalsIgnoreCase(month[j])) {
//                            day.add(code2.get(i));
//                            numMatchPerDay.add(numberOfMatch);
//                            numberOfMatch = 0;
//                        }
//                        if(repeatIndex==5) repeatIndex = 1;
//                        if(repeatIndex==1)
//                        {
//                            statusMatch.add(code2.get(i+index));
//                            numberOfMatch++;
//                        }
//                        if(repeatIndex==2) matchLeft.add(code2.get(i+index));
//                        if(repeatIndex==3) score.add(code2.get(i+index));    //score
//                        if(repeatIndex==4) matchRight.add(code2.get(i+index));
//                        repeatIndex++;
//                    }
//                }
//            }
//        }
//    }
//
//    public void madeScore(){
//        for(int i=0;i<score.size();i++)
//        {
//            String scoreMatch = score.get(i);
//            String keep = "";
//            for(int j=0;j<scoreMatch.length();j++)
//            {
//                if(scoreMatch.charAt(j)=='-')
//                {
//                    matchLeft.add(keep);
//                    keep = "";
//                }
//                else if(scoreMatch.charAt(j)==' '){}
//                else if(j==scoreMatch.length()-1){
//                    keep +=scoreMatch.charAt(j);
//                    matchRight.add(keep);
//                }
//                else{
//                    keep += scoreMatch.charAt(j);
//                }
//            }
//        }
//    }

    /**
     * get match from code2
     */
    private void callMatch()
    {
        //get word that contain with -vs-
        for (int i = 0; i < code2.size(); i++) {
            List<String> checker = new ArrayList<String>();
            checker.add("-vs-");
            if (code2.get(i).contains(checker.get(0))){
                code3.add(code2.get(i));
            }
        }
        //get home team to matchLeft and get away team to matchRight
        for(int i=0; i <code3.size() ; i++)
        {
            String text = code3.get(i);
            String string = "";
            String string2 = "";
            boolean stop = false;
            int left = 0;
            int right = 0;
            for (int j = 0; j < text.length(); j++) {
                if (text.charAt(j) == '-' && text.charAt(j+1) == 'v' && text.charAt(j+2) == 's' && text.charAt(j+3) == '-')
                {
                    left = j;
                    right = j+3;
                }
            }
            for (int j = left-1; j >=0 ; j--) {
                if (text.charAt(j) == '/') {
                    stop = true;
                }
                if(!stop){
                    if(text.charAt(j)=='-')
                        string += ' ';
                    else
                        string += text.charAt(j);
                }
            }

            // sort string
            String keep = "";
            for (int j=string.length()-1; j>=0 ; j--) {
                keep += string.charAt(j);
            }
            string = keep;

            stop = false;
            for (int j = right+1; j<text.length() ; j++) {
                if (text.charAt(j) == '/') {
                    stop = true;
                }
                if(!stop){
                    if(text.charAt(j)=='-')
                        string2 += ' ';
                    else
                        string2 += text.charAt(j);
                }
            }
            matchLeft.add(string);
            matchRight.add(string2);
        }
    }

    /**
     * get statusMatch from code2
     */
    private void callStatusMatch()
    {
        for(int i=0; i<code2.size(); i++)
        {
            List<String> dummies = new ArrayList<String>();
            dummies.add("class=\"min\">");
            if (code2.get(i).contains(dummies.get(0)))
            {
                String keep = "";
                String string = "";
                keep = code2.get(i+1).toString();
                if(keep.equalsIgnoreCase("FT"))
                    statusMatch.add(code2.get(i+1));
                else
                {
                    String hour = "";
                    String minute = "";
                    boolean stop = false;
                    for(int j=0; j<keep.length(); j++)
                    {
                        if(keep.charAt(j)==':')
                            stop = true;
                        else if(stop)
                            minute += keep.charAt(j);
                        else
                            hour += keep.charAt(j);
                    }
                    int sent = Integer.parseInt(hour);
                    sent += 7;
                    if(sent>=24)
                        sent %= 24;
                    if(sent>=10)
                        string += Integer.toString(sent);
                    else
                        string += "0"+Integer.toString(sent);
                    string += ":"+minute;
                    statusMatch.add(string);
                }
            }
        }
    }

    /**
     * get score from code2
     */
    private void callScore()
    {
        for(int i=0; i<code2.size(); i++)
        {
            List<String> dummies = new ArrayList<String>();
            dummies.add("class=\"scorelink\">");
            String string = "";
            if (code2.get(i).contains(dummies.get(0)))
            {
                String keep = code2.get(i);
                for(int j=0; j<keep.length(); j++)
                {
                    String keep2 = "";
                    if(keep.charAt(j)=='>')
                    {
                        for(int k=j+1; k<keep.length(); k++)
                        {
                            keep2 += keep.charAt(k);
                        }
                        string += keep2;
                    }

                }
                string += code2.get(i+1);
                keep = code2.get(i+2);
                boolean stop = false;
                for(int j=0; j<keep.length(); j++)
                {
                    String keep2 = "";
                    if(keep.charAt(j)=='<')
                        stop = true;
                    if(!stop)
                        keep2 += keep.charAt(j);
                    string += keep2;

                }
                boolean change = false;
                String home = "";
                String away = "";
                for(int j=0; j<string.length(); j++)
                {
                    if(string.charAt(j)=='-') {
                        change = true;
                    }
                    else {
                        if (!change)
                            home += string.charAt(j);
                        else
                            away += string.charAt(j);
                    }
                }
                scoreLeft.add(home);
                scoreRight.add(away);
            }
        }
    }

    /**
     * get date that matches play from code2
     */
    private void callDay()
    {
        for(int i=0; i<code2.size(); i++)
        {
            List<String> dummies = new ArrayList<String>();
            dummies.add("fs11\">");
            String string = "";
            if (code2.get(i).contains(dummies.get(0)))
            {
                string += code2.get(i+1);
                string += " ";
                string += code2.get(i+2);
                day.add(string);
            }
        }
    }

    /**
     * test all info from url
     */
    public void testInfo()
    {
//        System.out.println(code.get(0));
        //Test code
//        for(int i=0; i<=code2.size(); i++)
//        {
//            System.out.println(code2.get(i));
//        }
//        System.out.println(code.get(0));
        //Test matchLeft
//        for(int i=0; i<matchLeft.size(); i++)
//        {
//            System.out.println(matchLeft.get(i));
//        }
//        //Test matchRight
//        for(int i=0; i<matchRight.size(); i++)
//        {
//            System.out.println(matchRight.get(i));
//        }
//        //Test day
//        for(int i=0; i<day.size(); i++)
//        {
//            System.out.println(day.get(i));
//        }
//        //Test statusMatch
//        for(int i=0; i<statusMatch.size(); i++)
//        {
//            System.out.println(statusMatch.get(i));
//        }
//        //Test score
//        for(int i=0; i<scoreLeft.size(); i++)
//        {
//            System.out.println(scoreLeft.get(i)+"-"+scoreRight.get(i));
//        }
    }

    public void playSound(){
        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.click);
        mp.start();
    }
}