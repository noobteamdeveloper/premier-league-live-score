package com.example.tonystark.premierleaguelivescore;

import java.util.ArrayList;
import java.util.List;

public class LiveStorage{

    private List<Live> lives;

    private static LiveStorage instance;

    private LiveStorage() { lives = new ArrayList<Live>(); }

    public static LiveStorage getInstance() {
        if(instance == null) {
            instance = new LiveStorage();
        }
        return instance;
    }

    public void saveLive(Live live) { lives.add(live); }

    public List<Live> loadLives() {
        return lives;
    }

}