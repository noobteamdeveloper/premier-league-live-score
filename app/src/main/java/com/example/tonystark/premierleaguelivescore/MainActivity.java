package com.example.tonystark.premierleaguelivescore;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends Activity {

    private ImageButton liveButton;
    private ImageButton tableButton;
    private ImageButton topscorerButton;
    private ImageButton newsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
    }

    protected void onStart() {
        super.onStart();
        liveButton.setImageResource(R.drawable.livescore3);
        tableButton.setImageResource(R.drawable.table);
        topscorerButton.setImageResource(R.drawable.topscorer);
        newsButton.setImageResource(R.drawable.news);
    }

    private void initComponents()
    {
        liveButton = (ImageButton) findViewById(R.id.livescoreButton);
        liveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                liveButton.setImageResource(R.drawable.livescoreclick);
                playSound();
                Intent intent = new Intent(MainActivity.this, LiveActivity.class);
                startActivity(intent);
            }
        });
        tableButton = (ImageButton) findViewById(R.id.tableButton);
        tableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tableButton.setImageResource(R.drawable.tableclick);
                playSound();
                Intent intent = new Intent(MainActivity.this, TableActivity.class);
                startActivity(intent);
            }
        });
        topscorerButton = (ImageButton) findViewById(R.id.topscorerButton);
        topscorerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                topscorerButton.setImageResource(R.drawable.topscorerclick);
                playSound();
                Intent intent = new Intent(MainActivity.this, TopscorerActivity.class);
                startActivity(intent);
            }
        });
        newsButton = (ImageButton) findViewById(R.id.newsButton);
        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newsButton.setImageResource(R.drawable.newsclick);
                playSound();
                Intent intent = new Intent(MainActivity.this, NewsActivity.class);
                startActivity(intent);
            }
        });
        //check internet status
        if(!isInternetconnected())
        {

            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("ERROR");

            alertDialog.setMessage("Cannot connected to internet. please try to connected to internet   ");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }

    public boolean isInternetconnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public void playSound(){
        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.click);
        mp.start();
    }
}

