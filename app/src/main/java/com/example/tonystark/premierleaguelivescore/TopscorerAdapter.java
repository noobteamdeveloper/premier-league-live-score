package com.example.tonystark.premierleaguelivescore;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

public class TopscorerAdapter extends ArrayAdapter<Topscorer> {

    public TopscorerAdapter(Context context, int resource, List<Topscorer> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if(v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.topscorer_cell, null);
        }

        TextView player = (TextView)v.findViewById(R.id.player);
        TextView team = (TextView)v.findViewById(R.id.team);
        TextView goal = (TextView)v.findViewById(R.id.goal);

        Topscorer topscorer = getItem(position);
        player.setText(topscorer.getPlayer());
        team.setText(topscorer.getTeam());
        goal.setText(topscorer.getGoal());

        return v;
    }
}
