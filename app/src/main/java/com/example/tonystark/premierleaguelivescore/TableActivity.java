package com.example.tonystark.premierleaguelivescore;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

public class TableActivity extends Activity {

    private WebView webview;
    private ImageButton liveButton;
    private ImageButton topscorerButton;
    private ImageButton newsButton;
    private ImageButton resetButton;
    private TextView statusConnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_page);
        initComponents();
    }

    protected void onStart() {
        super.onStart();
        liveButton.setImageResource(R.drawable.livescore3);
        topscorerButton.setImageResource(R.drawable.topscorer);
        resetButton.setImageResource(R.drawable.refresh);
    }

    private void initComponents()
    {
        liveButton = (ImageButton) findViewById(R.id.livescoreButton);
        liveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                liveButton.setImageResource(R.drawable.livescoreclick);
                playSound();
                Intent intent = new Intent(TableActivity.this, LiveActivity.class);
                startActivity(intent);
            }
        });
        topscorerButton = (ImageButton) findViewById(R.id.topscorerButton);
        topscorerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                topscorerButton.setImageResource(R.drawable.topscorerclick);
                playSound();
                Intent intent = new Intent(TableActivity.this, TopscorerActivity.class);
                startActivity(intent);
            }
        });
        newsButton = (ImageButton) findViewById(R.id.newsButton);
        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newsButton.setImageResource(R.drawable.newsclick);
                playSound();
                Intent intent = new Intent(TableActivity.this, NewsActivity.class);
                startActivity(intent);
            }
        });
        if(isInternetconnected()) {
            statusConnect = (TextView) findViewById(R.id.statusConnection2);
            statusConnect.setText("INTERNET"+"\n"+"CONNECTED");
            openWebsite();
            resetButton = (ImageButton) findViewById(R.id.refreshButton2);
            resetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    playSound();
                    resetButton.setImageResource(R.drawable.refreshclick);
                    openWebsite();
                }
            });
        }
        else
        {
            AlertDialog alertDialog = new AlertDialog.Builder(TableActivity.this).create();
            alertDialog.setTitle("ERROR");
            alertDialog.setMessage("Cannot connected to internet. please try to connected to internet   ");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            statusConnect = (TextView) findViewById(R.id.statusConnection2);
            statusConnect.setText("NO INTERNET" + "\n" + "CONNECTION");
            resetButton = (ImageButton) findViewById(R.id.refreshButton2);
            resetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    resetButton.setImageResource(R.drawable.refreshclick);
                    playSound();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            });
        }
    }

    private void openWebsite(){
//        webview = (WebView) findViewById(R.id.web_view);
//        webview.setWebViewClient(new WebViewClient());
//        webview.getSettings().setJavaScriptEnabled(true);
//        webview.loadUrl("https://www.google.co.th");
        Uri uri = Uri.parse("http://www.sportinglife.com/football/premier-league/table");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(launchBrowser);
    }

    public boolean isInternetconnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public void playSound(){
        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.click);
        mp.start();
    }
}
