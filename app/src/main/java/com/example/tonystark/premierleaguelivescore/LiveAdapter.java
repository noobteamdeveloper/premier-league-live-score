package com.example.tonystark.premierleaguelivescore;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class LiveAdapter extends ArrayAdapter<Live> {

    public LiveAdapter(Context context, int resource, List<Live> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if(v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.live_cell, null);
        }

        TextView matchleft = (TextView)v.findViewById(R.id.match_left);
        TextView matchright = (TextView)v.findViewById(R.id.match_right);
        TextView scoreleft = (TextView)v.findViewById(R.id.score_left);
        TextView scoreright = (TextView)v.findViewById(R.id.score_right);
        TextView status = (TextView)v.findViewById(R.id.status);

        Live live = getItem(position);
        matchleft.setText(live.getMatchLeft());
        matchright.setText(live.getMatchRight());
        scoreleft.setText(live.getScoreLeft());
        scoreright.setText(live.getScoreRight());
        status.setText(live.getStatus());

        return v;
    }
}
