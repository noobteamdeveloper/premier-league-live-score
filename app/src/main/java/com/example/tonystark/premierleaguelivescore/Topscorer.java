package com.example.tonystark.premierleaguelivescore;

import java.io.Serializable;

public class Topscorer implements Serializable {

    private String player;
    private String team;
    private String goal;

    public Topscorer(String player,String team,String goal) {
        this.player = player;
        this.team = team;
        this.goal = goal;
    }

    public String getPlayer() { return player; }

    public String getTeam() { return team; }

    public String getGoal() { return goal; }

    @Override
    public String toString() {
        return "Topscorer{" +
                "player='" + player + '\'' +
                ", team='" + team + '\'' +
                ", goal='" + goal + '\'' +
                '}';
    }
}